package ru.kopylov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DataEndpointTest {

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpointService().getDataEndpointPort();

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void setUp() {
        try {
            adminToken = userEndpoint.loginUser("test", "test");
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
        try {
            userEndpoint.persistUser("testUser", "testUser");
            userToken = userEndpoint.loginUser("testUser", "testUser");
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception_Exception {
        @Nullable final UserDto user = userEndpoint.getUserProfile(userToken);
        @Nullable final UserDto admin = userEndpoint.getUserProfile(adminToken);
        userEndpoint.removeUser(adminToken, user.getId());
        userEndpoint.removeSession(adminToken, admin.getId());
    }

    @Test(expected = Exception_Exception.class)
    public void saveDataBinByUser() throws Exception_Exception {
        dataEndpoint.saveDataBin(userToken);
    }

    @Test(expected = Exception_Exception.class)
    public void loadDataBinByUser() throws Exception_Exception {
        dataEndpoint.loadDataBin(userToken);
    }

}