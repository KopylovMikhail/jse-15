package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.endpoint.State;
import ru.kopylov.tm.endpoint.TaskDto;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@NoArgsConstructor
public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UPDATE]");
        @NotNull final List<TaskDto> tasks = bootstrap.getTaskEndpoint().getTaskList(bootstrap.getToken(), null);
        CommandUtil.printTaskListWithParam(tasks);
        System.out.println("ENTER EXISTING TASK NUMBER:");
        @Nullable String terminalCommand = bootstrap.getTerminalService().getReadLine();
        if (terminalCommand == null || terminalCommand.isEmpty()) {
            System.out.println("Number is empty.");
            return;
        }
        @NotNull final Integer taskNumber = Integer.parseInt(terminalCommand);
        System.out.println("ENTER NEW TASK NAME OR PRESS [ENTER] TO SKIP:");
        @Nullable final String taskName = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER TASK DESCRIPTION OR PRESS [ENTER] TO SKIP:");
        @Nullable final String taskDescription = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER TASK DATE START (DD.MM.YYYY) OR PRESS [ENTER] TO SKIP:");
        @Nullable final String taskDateStart = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER TASK DATE FINISH (DD.MM.YYYY) OR PRESS [ENTER] TO SKIP:");
        @Nullable final String taskDateFinish = bootstrap.getTerminalService().getReadLine();
        @NotNull final State[] states = bootstrap.getProjectEndpoint().getStateList().toArray(new State[0]);
        int count = 1;
        for (State state : states) {
            System.out.println(count++ + ". " + state.value());
        }
        System.out.println("ENTER TASK STATE NUMBER OR PRESS [ENTER] TO SKIP:");
        terminalCommand = bootstrap.getTerminalService().getReadLine();
        @Nullable Integer stateNumber = null;
        if (terminalCommand != null && !terminalCommand.isEmpty()) stateNumber = Integer.parseInt(terminalCommand);
        final boolean updateSuccess = bootstrap.getTaskEndpoint().updateTask(
                bootstrap.getToken(),
                taskNumber,
                taskName,
                taskDescription,
                taskDateStart,
                taskDateFinish,
                stateNumber
        );
        if (updateSuccess)
            System.out.println("[TASK HAS BEEN UPDATED]\n");
        else System.out.println("Such a task does not exist or name is empty.");
    }

}
