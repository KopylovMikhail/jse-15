package ru.kopylov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtil {

    @Nullable
    private static final String SALT = "OPI&L%$4kj";

    @NotNull
    public static String hash(@NotNull String text) {
        text = SALT + text + SALT;
        @Nullable MessageDigest md = null;
        byte[] digest = new byte[0];
        try {
            md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(text.getBytes());
            digest = md.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        @NotNull final BigInteger bigInt = new BigInteger(1,digest);
        @NotNull final String hashtext = bigInt.toString(16);
        return hashtext;
    }
}
