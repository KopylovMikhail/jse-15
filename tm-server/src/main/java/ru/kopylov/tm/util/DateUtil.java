package ru.kopylov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {

    @NotNull
    private static final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static Date stringToDate(@NotNull String dateString) throws ParseException {
        return formatter.parse(dateString);
    }

    @NotNull
    public static String dateToString(@NotNull Date date) {
        return formatter.format(date);
    }

}
