package ru.kopylov.tm;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.context.Bootstrap;

/**
 * @author Mikhail Kopylov
 * task/project manager
 */

public final class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
