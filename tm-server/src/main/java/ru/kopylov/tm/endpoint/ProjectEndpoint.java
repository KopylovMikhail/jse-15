package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.endpoint.IProjectEndpoint;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.dto.ProjectDto;
import ru.kopylov.tm.dto.TaskDto;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.util.EntityToDtoUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final String url = primaryUrl + this.getClass().getSimpleName() + "?wsdl";

    public ProjectEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ServiceLocator bootstrap
    ) {
        super(sessionService, bootstrap);
    }

    @WebMethod
    public void clearProject(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        bootstrap.getProjectService().removeAll(session.getUser().getId());
    }

    @WebMethod
    public boolean createProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectName") @Nullable final String projectName
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().persist(session.getUser().getId(), projectName);
    }

    @NotNull
    @WebMethod
    public List<ProjectDto> findProjectContent(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "findWord") @Nullable final String findWord
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        return EntityToDtoUtil.getProjectList(bootstrap.getProjectService().findByContent(findWord));
    }

    @Nullable
    @WebMethod
    public List<ProjectDto> getProjectList(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "typeSort") @Nullable final String typeSort
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        return EntityToDtoUtil.getProjectList(bootstrap.getProjectService().findAll(session.getUser().getId(), typeSort));
    }

    @WebMethod
    public boolean removeProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().remove(session.getUser().getId(), projectNumber);
    }

    @WebMethod
    public boolean setProjectTask(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber,
            @WebParam(name = "taskNumber") @NotNull final Integer taskNumber
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().setTask(session.getUser().getId(), projectNumber, taskNumber);
    }

    @NotNull
    @WebMethod
    public List<TaskDto> getProjectTaskList(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        return EntityToDtoUtil.getTaskList(bootstrap.getProjectService().getTaskList(session.getUser().getId(), projectNumber));
    }

    @WebMethod
    public boolean updateProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "projectDescription") @Nullable final String projectDescription,
            @WebParam(name = "projectDateStart") @Nullable final String projectDateStart,
            @WebParam(name = "projectDateFinish") @Nullable final String projectDateFinish,
            @WebParam(name = "stateNumber") @Nullable final Integer stateNumber
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        return bootstrap.getProjectService().merge(
                session.getUser().getId(),
                projectNumber,
                projectName,
                projectDescription,
                projectDateStart,
                projectDateFinish,
                stateNumber
        );
    }

    @NotNull
    @WebMethod
    public State[] getStateList() {
        return State.values();
    }

}
