package ru.kopylov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum TypeRole {

    ADMIN("administrator"),
    USER("user");

    @NotNull
    private final String displayName;

    TypeRole(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
