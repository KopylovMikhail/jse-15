package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.dto.AbstractDto;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractDto> {

    protected ServiceLocator bootstrap;

    public AbstractService(@NotNull final ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }

}
