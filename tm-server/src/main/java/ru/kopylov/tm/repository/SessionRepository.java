package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.api.repository.ISessionRepository;
import ru.kopylov.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager manager) {
        super(manager);
    }

    @Override
    public void remove(@NotNull String id) {
        manager.remove(manager.find(Session.class, id));
    }

    @NotNull
    @Override
    public Session findOne(@NotNull String id) {
        return manager.find(Session.class, id);
    }

    public void removeByUserId(@NotNull final String userId) {
        @NotNull final List<Session> sessions = manager
                .createQuery("select s from Session s where s.user.id = :userId", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .setParameter("userId", userId).getResultList();
        for (@NotNull final Session session : sessions) {
            manager.remove(session);
        }
    }

}
