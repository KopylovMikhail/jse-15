package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface IPropertyService {

    void init() throws IOException;

    @NotNull
    String getPort();

    @NotNull
    String getHost();

    @NotNull
    String getSalt();

    int getCycle();

    long getLifeTime();

    @NotNull
    String getDbDriver();

    @NotNull
    String getDbHost();

    @NotNull
    String getDbLogin();

    @NotNull
    String getDbPassword();

    @NotNull
    String getSecretKey();

}
