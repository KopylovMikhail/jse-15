package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void persist(@NotNull Task task);

    void merge(@NotNull Task task);

    void remove(@NotNull String id);

    @NotNull
    Task findOne(@NotNull String id);

    @NotNull
    public List<Task> findAll();

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdOrderByDateStart(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdOrderByDateFinish(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdOrderByState(@NotNull String userId);

    void removeAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findByContent(@NotNull String content);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId);

    void setProjectId(
            @NotNull String projectId,
            @NotNull String taskId
    );

}
