package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void persist(@NotNull Project project);

    void merge(@NotNull Project project);

    void remove(@NotNull String id);

    @NotNull
    public Project findOne(@NotNull String id);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserIdOrderByDateStart(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserIdOrderByDateFinish(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserIdOrderByState(@NotNull String userId);

    void removeAllByUserId(@NotNull String userId);

    @NotNull
    List<Project> findByContent(@NotNull String content);

}
