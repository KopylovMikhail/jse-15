package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    void persist(@NotNull User user);

    void merge(@NotNull User user);

    void remove(@NotNull String id);

    @NotNull
    User findOne(@NotNull String id);

    @NotNull
    User findOne(@NotNull String login, @NotNull String password);

    @NotNull
    List<User> findAll();

}
